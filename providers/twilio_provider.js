const Sid = 'AC0fdc94b6eef44d9124c2586b78dc58ac';
const authToken = 'ae212eb3e8f439cbb9572808f2d7abd1';
const clientTwilio = require('twilio')(Sid, authToken);

const cron = require("node-cron");

const Users = require('../models/users');
const Numbers = require('../models/numbers');

const hostname = 'http://103.19.16.62/';

function removeDuplicates(arr){
    let unique_array = []
    for(let i = 0;i < arr.length; i++){
        if(unique_array.indexOf(arr[i]) == -1){
            unique_array.push(arr[i])
        }
    }
    return unique_array
}

function callAll(data, xml_url) {
    let num = data.pop();
    let total = data.length;

    console.log('numbers pop : ' + num);
    console.log('xml : ' + xml_url);

    clientTwilio.calls
        .create({
            url: xml_url,
            to: num,
            from: '+13012347298',
        },
        (err, call) => {
            console.log(err);
            if(err) throw err;
            
            if(total > 0) {
                    callAll(data, xml_url);
            }
            else {
                console.log('called all');
            }
        }
    );

}

module.exports.doTest = function(param) {

    let uid_array = [];
    // let xml_url = hostname + 'uploads/sound_xml/1.xml'; // xml url
    let xml_url = 'http://103.19.16.62/sound_xml/1.xml'; // xml url

    uid_array.push(param.uid);
        
    Numbers.loadNumbersByUID(uid_array, (err, nres) => {
        num_array = [];
        for(let i = 0; i < nres.length; i++) {
            num_array.push(nres[i].phone_number);
        }
        
        num_array = removeDuplicates(num_array);
        
        console.log('test num : ' + JSON.stringify(num_array));
                    
        if(num_array.length != 0) {
            callAll(num_array, xml_url);
        }
                    
    });
}

module.exports.startAction = function() {

    console.log(hostname);

    cron.schedule('*/1 * * * * *', function() {

        let xml_url = ''; // xml url

        // console.log('~~~');
        let now = new Date();
        if ((now.getUTCHours() == '0') && (now.getUTCMinutes() == '0') && (now.getUTCSeconds() == '0')) {
            xml_url = hostname + 'sound_xml/1.xml';
        }
        if ((now.getUTCHours() == '6') && (now.getUTCMinutes() == '0') && (now.getUTCSeconds() == '0')) {
            xml_url = hostname + 'sound_xml/2.xml';
        }
        if ((now.getUTCHours() == '12') && (now.getUTCMinutes() == '0') && (now.getUTCSeconds() == '0')) {
            xml_url = hostname + 'sound_xml/3.xml';
        }
        if ((now.getUTCHours() == '24') && (now.getUTCMinutes() == '0') && (now.getUTCSeconds() == '0')) {
            xml_url = hostname + 'sound_xml/4.xml';
        }

        if(xml_url != '') {
            Users.loadAvailableUsers({}, (err, results) => {

                let uid_array = [];
                for(let i = 0; i < results.length; i++) {
                    uid_array.push(results[i]._id.toString());
                }
        
                Numbers.loadNumbersByUID(uid_array, (err, nres) => {
                    num_array = [];
                    for(let i = 0; i < nres.length; i++) {
                        num_array.push(nres[i].phone_number);
                    }
        
                    num_array = removeDuplicates(num_array);
        
                    console.log('num : ' + JSON.stringify(num_array));
                    
                    if(num_array.length != 0) {
                        callAll(num_array, xml_url);
                    }
                    
                });
            });
        }
    });
}