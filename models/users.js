const mongoose = require('mongoose');
// Users Schema
const UsersSchema = mongoose.Schema({
    username: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    status: {
        type: Boolean
    }
}, {collection: 'users'});

const Users = module.exports = mongoose.model('Users', UsersSchema);

module.exports.addUsers = function(param, callback) {

    console.log('model : ' + JSON.stringify(param));
    let udoc = new Users(param);
    udoc.save(callback);
}

module.exports.updateUsers = function(param, callback) {

    console.log('u model : ' + JSON.stringify(param));
    Users.findOneAndUpdate({_id: param._id}, param, callback);
}

module.exports.checkUsers = function(param, callback) {

    console.log('check model : ' + JSON.stringify(param));
    Users.find({email: param.email, password: param.password}, callback);
}

module.exports.loadAvailableUsers = function(param, callback) {
    Users.find({status: true}, callback);
}

/*
module.exports.loadUsersByIndex = function(param, callback) {
    console.log("index : " + param.page);

    Users.find({}, callback)
         .skip((10 * (param.page + 1)) - 10)
         .limit(10);
}

module.exports.getUsersLen = function(param, callback) {
    Users.find({})
        .countDocuments(callback);
}

module.exports.deleteItem = function(param, callback) {
    Users.find({}, callback)
         .skip((10 * (param.page + 1)) - 10)
         .limit(10);
}
module.exports.rdeleteItem = function(param, callback) {
    deleteAll(param, callback);
}*/