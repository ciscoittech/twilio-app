const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Users = require('../models/users');
const UsersProvider = require('../providers/users_provider');


router.post('/signup', (req, res, next) => {
    UsersProvider.addUsers(req.body, res);
});
router.post('/login', (req, res, next) => {
    UsersProvider.checkUsers(req.body, res);
});
router.post('/update', (req, res, next) => {
    UsersProvider.updateUsers(req.body, res);
});
/*
router.post('/get_users_len', (req, res, next) => {
    UsersProvider.getUsersLen(res);
});

router.post('/delete_item', (req, res, next) => {
    UsersProvider.deleteItem(req.body, res);
});
*/
module.exports = router;