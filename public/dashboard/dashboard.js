let gnumbers = [];

$( document ).ready(function() {

    let user = localStorage.getItem('user');
    let userobj = JSON.parse(user);

    $('.title').empty();
    $('.title').append(userobj.username + "'s Dashboard");

    if(user) {
        loadNumbers(userobj);
        setStatus(userobj);
    } else {
        window.location.href = "/";
    }

    $('.logout').click(function(){
        localStorage.removeItem('user');
        window.location.href = "/";
    });

    $('.addnumber').click(function() {
        if($('.pnumber').val() != '') {

            $.post('/numbers/add',
            {
                uid: userobj._id,
                phone_number: $('.pnumber').val()
            }, function(data, status) {
                if(data.result == true) {
                    alert('added successfully');
                    loadNumbers(userobj);
                }
            });
        } else {
            alert('fill the phone number');
        }
    });

});

function doTest() {
    let user = localStorage.getItem('user');
    let userobj = JSON.parse(user);

    $.post('/twilio/test',
    {
        uid: userobj._id,

    }, function(data, status) {
    });

    alert('testing..');
}

function changedStatus(str) {

    let user = localStorage.getItem('user');
    let userobj = JSON.parse(user);

    let val;
    if(str == 'on') {
        val = true;
    }
    if(str == 'off') {
        val = false;
    }

    $.post('/users/update',
    {
        _id: userobj._id,
        status: val

    }, function(data, status) {
        localStorage.setItem('user', JSON.stringify(data.data));
        // setStatus(data.data);
    });
}

function remove(index) {

    let user = localStorage.getItem('user');
    let userobj = JSON.parse(user);

    $.post('/numbers/remove',
    {
        _id: gnumbers[index]._id,
    }, function(data, status) {
        loadNumbers(userobj);
    });
}

function setStatus(userobj) {
    if(userobj.status == true) {
        $('#on').parent('.btn').addClass('active'); 
        $('#off').parent('.btn').removeClass('active'); 
    } else {
        $('#off').parent('.btn').addClass('active'); 
        $('#on').parent('.btn').removeClass('active');
    }
}

function loadNumbers(userobj) {
    $("#content").empty();

    $.post('/numbers/load',
    {
        uid: userobj._id,
    }, function(data, status) {
        console.log('data : ' + JSON.stringify(data.data));
        gnumbers = data.data;
        for(let i = 0; i < data.data.length; i++) {
            $("#content").append("<tr><td>" + data.data[i].phone_number + "</td>" + 
                                 "<td><i id='" + i + "' class='fa fa-times-circle' aria-hidden='true' onclick='remove("+ i +")'></i></td>");
        }
    });
}