const mongoose = require('mongoose');
// Users Schema
const NumbersSchema = mongoose.Schema({
    uid: {
        type: Object
    },
    phone_number: {
        type: String
    }
}, {collection: 'numbers'});

const Numbers = module.exports = mongoose.model('Numbers', NumbersSchema);

module.exports.addNumbers = function(param, callback) {
    console.log('n model : ' + JSON.stringify(param));
    let ndoc = new Numbers(param);
    ndoc.save(callback);
}

module.exports.loadNumbers = function(param, callback) {
    console.log('l model : ' + JSON.stringify(param));
    Numbers.find({uid: param.uid}, callback);
}


module.exports.removeNumbers = function(param, callback) {
    console.log('r model : ' + JSON.stringify(param));
    Numbers.findOneAndRemove({_id: param._id}, callback);
}

module.exports.loadNumbersByUID = function(param, callback) {
    Numbers.find({uid: {$in: param}}, callback);
}
