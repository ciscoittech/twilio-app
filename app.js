const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const multipart = require('connect-multiparty');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

const users_provider = require('./providers/users_provider');
const numbers_provider = require('./providers/numbers_provider');
const twilio_provider = require('./providers/twilio_provider');

// Connect To Database - AWESOME
mongoose.connect(config.database);
// On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database '+ config.database);
});
// On Error
mongoose.connection.on('error', (err) => {
    console.log('Connected error '+ err);
});

const app = express();

// Routes
const users = require('./routes/users');
const numbers = require('./routes/numbers');
const twilio = require('./routes/twilio');

// Port Number
const port = process.env.PORT || 8080;

// CORS Middleware
app.use(cors());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json()); // parse json
app.use(bodyParser.urlencoded({extended: true}) ); //parse url enconded

// app.use(multipart());

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// set path for the uploaded images
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));


// Use Route
app.use('/users', users);
app.use('/numbers', numbers);
app.use('/twilio', twilio);
// Index Route

// set root path when build as production
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index/index.html'));
});
app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/login/login.html'));
});
app.get('/signup', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/signup/signup.html'));
});
app.get('/dashboard', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/dashboard/dashboard.html'));
});


// Start Server
app.listen(port, () => {
    console.log('Server started on port : ' + port);
    twilio_provider.startAction();
});