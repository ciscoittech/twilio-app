$( document ).ready(function() {
    $("#btnSubmit").click(function(event) {

        // Fetch form to apply custom Bootstrap validation
        var form = $("#myForm")
    
        if (form[0].checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        
        form.addClass('was-validated');
        // Perform ajax submit here...

        if (form[0].checkValidity() === true) {
            $.post('/users/signup',
            {
                username: $('#username').val(),
                email: $('#email').val(),
                password: $('#password').val(),
                status: true
            }, function(data, status) {
                console.log(data.result)
                if(data.result) {
                    window.location.href = "/login";
                }
            });
        }

        return false;
        
    });

    $(".back").click(function() {
        window.location.href = "/";
    });
});

function doSignup() {
    alert();
}

