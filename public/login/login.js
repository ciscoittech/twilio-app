$( document ).ready(function() {
    $(".errorspan").hide();

    $("#btnSubmit").click(function(event) {

        // Fetch form to apply custom Bootstrap validation
        var form = $("#myForm")
    
        if (form[0].checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        
        form.addClass('was-validated');
        // Perform ajax submit here...

        if (form[0].checkValidity() === true) {
            $.post('/users/login',
            {
                email: $('#email').val(),
                password: $('#password').val()
            }, function(data, status) {
                if(data.result == true) {
                    localStorage.setItem('user', JSON.stringify(data.data));
                    window.location.href = "/dashboard";
                } else {
                    $(".errorspan").show();
                }
            });
        }

        return false;
        
    });

    $(".back").click(function() {
        window.location.href = "/";
    });
});

